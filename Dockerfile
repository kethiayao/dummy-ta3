FROM registry.gitlab.com/datadrivendiscovery/images/base:ubuntu-bionic-python36

ADD . /TA3/

WORKDIR /TA3

RUN pip3 install .
