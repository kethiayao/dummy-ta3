#!/bin/bash -e

python3 -m dummy_ta3.dummy_ta3 --problem-path "$D3MPROBLEMPATH" --datasets-dir "$D3MINPUTDIR" --time-bound "$D3MTIMEOUT" "$@"

find "$D3MOUTPUTDIR" -ls

# We check that any pipeline has been exported. This will exit
# the script with non-zero exit code if directory is empty.
find "$D3MOUTPUTDIR/pipelines_ranked" -mindepth 1 | read

# Inside TA3-TA2 CI we want to store pipelines as results.
# Results are then stored as CI artifacts.
if [ -e /results ]; then
    cp -a "$D3MOUTPUTDIR/pipelines_ranked" /results/pipelines_ranked

    for dir in pipelines_scored pipelines_searched pipeline_runs additional_inputs; do
        if [ -e "$D3MOUTPUTDIR/$dir" ]; then
            cp -a "$D3MOUTPUTDIR/$dir" "/results/$dir"
        fi
    done
fi
