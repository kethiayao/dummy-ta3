#!/bin/bash -e

mkdir -p $D3MOUTPUTDIR/logs

python3 -m dummy_ta3.dummy_ta3 --problem-path "$D3MPROBLEMPATH" --datasets-dir "$D3MINPUTDIR" --time-bound "$D3MTIMEOUT" "$@" | tee -a $D3MOUTPUTDIR/logs/dummy-ta3.log

find "$D3MOUTPUTDIR" -ls

# We check that any pipeline has been exported. This will exit
# the script with non-zero exit code if directory is empty.
find "$D3MOUTPUTDIR/pipelines_ranked" -mindepth 1 | read
