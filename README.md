## Fast DummyTA3 iteration.

Install (using pip 19 or later):
```
pip3 install .
```

Usage

```
python3 -m dummy_ta3.dummy_ta3 -p problem_path -d datasets_dir -e host_address -t timebound
```

Example
```
python3 -m dummy_ta3.dummy_ta3 -p /datasets/seed_datasets_current/38_sick/38_sick_problem/problemDoc.json -d /datasets -e localhost -t 300
```

Note: `timeout` is in seconds for DummyTA3, while TA3-TA2 API uses minutes for its `time_bound` value.

# Testing Instruction
* Get the yaml pod definationf rom https://gitlab.com/datadrivendiscovery/dummy-ta3/tree/master/env_testing to a kubernetes environment (ie: the kubernetes jump server)
* Replace the ta2 image with your image id
* Run `kubectl exec -ti ta2-ta3-dummy-pod -c ta3-dummy bash`
you should have an environment with a ta2 container running at localhost:45042 and access to all the code in this repository on /TA3 directory
